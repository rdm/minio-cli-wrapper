# MinIO CLI Wrapper

A small package to wrap the
[MinIO Client](https://docs.min.io/docs/minio-client-complete-guide.html)
(`mc`) commands that are not provided by the [MinIO Python
SDK](https://docs.min.io/docs/python-client-api-reference.html) like
most of the `admin` functions. As it uses an individual and temporary
configuration folder with each instance it is concurrency save and does
not interfere with your personal settings.

# Setup

Install
[MinIO Client](https://docs.min.io/docs/minio-client-complete-guide.html)
from
https://docs.min.io/docs/minio-client-quickstart-guide.html#GNU/Linux
like described in the
[guide](https://docs.min.io/docs/minio-client-quickstart-guide.html#GNU/Linux).

Install the MinIO CLI Wrapper package or add the to `requirements.txt`:

```bash
pip install -i https://git.ufz.de/api/v4/projects/2959/packages/pypi/simple minio-cli-wrapper
```
`requirements.txt`:
```
-i https://git.ufz.de/api/v4/projects/2959/packages/pypi/simple
minio-cli-wrapper>=0.0.12
```

# Usage

Create a client instance:

```python
from minio_cli_wrapper.mc import Mc
mc = Mc(
    "localhost:9000",
    secure=False,
    access_key="minio",
    secret_key="minio123"
)
```

Create a MinIO user:

```python
mc.user_add('my-user', 'my_secret')
```

Create a IAM policy:

```python
mc.policy_add('my-policy-name', {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetBucketLocation",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::{bucket_name}".format(bucket_name='my-bucket-name')
            ]
        }
    ]
})
```

Assign a IAM policy to a user:

```python
mc.policy_set_user('my-user', 'my-policy-name')
```

Create a new service account for a user (this doesn't work until the
user has an IAM policy assigned):

```python
svc_creds = mc.create_service_account('my-user')
```

# Tests

There is an integration test tailored for an empty minio container:

```bash
./run-tests.sh
```
