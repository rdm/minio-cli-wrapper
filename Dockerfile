FROM debian:bullseye-slim as base

RUN apt-get -y update \
    && apt-get -y dist-upgrade \
    && apt-get -y --no-install-recommends install \
      python3 \
      python3-pkg-resources \
      ca-certificates \
      python3-pip \
      curl \
    && apt-get -y autoremove \
    && apt-get -y autoclean \
    && rm -rf /var/lib/apt

RUN pip install --upgrade pip && pip install minio

RUN curl https://dl.min.io/client/mc/release/linux-amd64/mc > /usr/local/bin/mc \
    && chmod a+x /usr/local/bin/mc