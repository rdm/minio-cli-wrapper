import argparse

import sys
sys.path.append('/code')

from minio_cli_wrapper.mc import Mc

parser = argparse.ArgumentParser()
parser.add_argument('--url', default='minio:9000')
args = parser.parse_args()

# Docker container for testing docker run --rm -p 9000:9000 -p 9001:9001 -e
# MINIO_NOTIFY_KAFKA_ENABLE="on" -e MINIO_NOTIFY_KAFKA_BROKERS="kafka:9092" -e
# MINIO_NOTIFY_KAFKA_TOPIC="minio-bucket-notifications" minio/minio server /vol0 /vol1 /vol2
# /vol3 --console-address :9001

m = Mc(args.url, secure=False, access_key='minioadmin', secret_key='minioadmin')

m.user_add('test', 'testtesttest')
m.policy_add('test', {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetBucketLocation",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::{bucket_name}".format(bucket_name='test')
            ]
        }
    ]
})

m.policy_set_user('test', 'test')
m.create_service_account('test')
m.bucket_exists('test')
m.make_locked_bucket('test')
m.set_bucket_100y_retention('test')
m.enable_bucket_notification('test')
m.set_bucket_tags('test', {'foo': 'bar'})

m.policy_unset_user('test', 'test')

m.set_bucket_quota('test', '10gi')