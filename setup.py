import setuptools
import minio_cli_wrapper

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='minio_cli_wrapper',
    version=minio_cli_wrapper.__version__,
    author='Martin Abbrent',
    author_email='martin.abbrent@ufz.de',
    description='Wrapper to control MinIO commands that are not covered by the MinIO Python SDK',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://git.ufz.de/rdm-software/timeseries-management/minio-cli-wrapper',
    project_urls={
        "Bug Tracker": "https://git.ufz.de/rdm-software/timeseries-management/minio-cli-wrapper/-/issues"
    },
    license='HEESIL',
    packages=['minio_cli_wrapper'],
    install_requires=['minio'],
)
